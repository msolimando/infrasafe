#!/bin/sh
echo "Stopping AXIS service..."
for pid in `ps aux | grep 'python axis.py$' | grep -v "grep" | awk '{ print $2 }'`
do
	kill -9 $pid
done
